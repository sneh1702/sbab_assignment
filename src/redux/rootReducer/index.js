import {combineReducers} from 'redux';
import homeReducer from "../../home/reducer";

const RootReducer = combineReducers({
    homeReducer
});

export default RootReducer;