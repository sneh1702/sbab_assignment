import { createStore, applyMiddleware } from "redux";
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import RootReducer from "../rootReducer";

import { baseURL } from "../../config/apiConfig";

const client = axios.create({
    baseURL: baseURL,
    responseType: 'json'
});

const middleware = [axiosMiddleware(client)];

export default createStore(
    RootReducer,
    applyMiddleware(...middleware)
);