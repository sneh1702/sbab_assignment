import React from 'react';
import { shallow } from 'enzyme';
import Main from './';
import store from '../redux/store'

describe('Main Component', () => {
  it('should render correctly', () => {
    const component = shallow(<Main store={store}  />).dive().dive();
  
    expect(component).toMatchSnapshot();
  });
});