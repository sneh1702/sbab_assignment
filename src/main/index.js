import React from 'react';
import { Provider } from "react-redux";

import store from "../redux/store";
import'bootstrap/dist/css/bootstrap.css';

import Home from '../home';
import './styles/main.scss'

function Main() {
	return (
		<Provider store={store}>
			<div className="container main">
				<Home />
			</div>
		</Provider>
	);
}

export default Main;
