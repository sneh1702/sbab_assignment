import React from "react";
import random from "random-key";

export default function BusStopList(props) {
    const { busStopList } = props;
    const pageHeading = '10 SL Bus routes with the most number of Stops';
    const busStopListItems = getBusStopListItems(busStopList);

    return (
        <div>
            <p className='bus-list-page-heading'>{pageHeading}</p>
            {busStopListItems}
        </div>
    );
}

function getBusStopListItems(busStopList) {
    const result = [];
    busStopList.map((busLine) => {
        const currentObjectKey = Object.keys(busLine)[0];
        result.push(
            <div key={random.generate()}>
                <div className='bus-route-heading'> Line Number {currentObjectKey} has the following {busLine[currentObjectKey].length} stops:</div>
                <div className='row bus-stop-list'>
                    {busLine[Object.keys(busLine)[0]].map((stopName) => (                        
                        <p 
                            key={random.generate()}
                            className='col-sm-6 col-md-3'
                        >
                            {stopName}
                        </p>
                    ))}
                </div>
            </div>
        );

        return {};
    });

    return result;
}