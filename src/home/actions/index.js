import { apiKey } from "../../config/apiConfig";

export const GET_STOPS = 'GET_STOPS';
export const GET_STOPS_SUCCESS = 'GET_STOPS_SUCCESS';
export const GET_STOPS_FAIL = 'GET_STOPS_FAIL';

export const GET_STOPS_INFO = 'GET_STOPS_INFO';
export const GET_STOPS_INFO_SUCCESS = 'GET_STOPS_INFO_SUCCESS';
export const GET_STOPS_INFO_FAIL = 'GET_STOPS_INFO_FAIL';

export function getListOfStops() {
    return {
        type: GET_STOPS,
        payload: {
            request: {
                url: `/linedata.json?key=${apiKey}&model=jour&DefaultTransportModeCode=BUS`
            }
        }
    };
}

export function getStopsInfo() {
    return {
        type: GET_STOPS_INFO,
        payload: {
            request: {
                url: `/linedata.json?key=${apiKey}&model=stop&DefaultTransportModeCode=BUS`
            }
        }
    };
}