import React from 'react';
import { shallow } from 'enzyme';
import Home from '..';
import store from '../../redux/store'
import { listOfStops, stopsData } from "./testData/homeTestData";

describe('Home', () => {
    it('should render correctly without the data', () => {
        const component = shallow(<Home store={store} />).dive().dive();

        expect(component).toMatchSnapshot();
    });
    it('should render correctly if there is an error', () => {
        const component = shallow(<Home store={store} />).dive().dive();
        const errorText = 'Received an Error'

        component.setProps({
            error: errorText
        });

        expect(component.text()).toEqual(errorText)
        expect(component).toMatchSnapshot();
    });

    it('should render correctly with all the data being provided', () => {
        const component = shallow(<Home store={store} />).dive().dive();
        
        component.setProps({
            listOfStops: listOfStops,
            stopsData: stopsData
        })

        expect(component).toMatchSnapshot();
    });
});