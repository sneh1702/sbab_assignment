export const stopsData = [
    {
        "StopPointNumber": "10008",
        "StopPointName": "S:t Eriksgatan",
        "StopAreaNumber": "10008",
        "LocationNorthingCoordinate": "59.3346583312157",
        "LocationEastingCoordinate": "18.0315016515233",
        "ZoneShortName": "A",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2018-03-29 00:00:00.000",
        "ExistsFromDate": "2018-03-29 00:00:00.000"
    },
    {
        "StopPointNumber": "10009",
        "StopPointName": "S:t Eriksgatan",
        "StopAreaNumber": "10008",
        "LocationNorthingCoordinate": "59.3342500243393",
        "LocationEastingCoordinate": "18.0335615860470",
        "ZoneShortName": "A",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2018-03-29 00:00:00.000",
        "ExistsFromDate": "2018-03-29 00:00:00.000"
    },
    {
        "StopPointNumber": "10010",
        "StopPointName": "Frihamnens färjeterminal",
        "StopAreaNumber": "10010",
        "LocationNorthingCoordinate": "59.3425635205332",
        "LocationEastingCoordinate": "18.1189085925683",
        "ZoneShortName": "A",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2015-09-24 00:00:00.000",
        "ExistsFromDate": "2015-09-24 00:00:00.000"
    },
    {
        "StopPointNumber": "10011",
        "StopPointName": "Frihamnsporten",
        "StopAreaNumber": "10052",
        "LocationNorthingCoordinate": "59.3399094088355",
        "LocationEastingCoordinate": "18.1146321830924",
        "ZoneShortName": "A",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2014-06-03 00:00:00.000",
        "ExistsFromDate": "2014-06-03 00:00:00.000"
    },
    {
        "StopPointNumber": "10012",
        "StopPointName": "Celsiusgatan",
        "StopAreaNumber": "10012",
        "LocationNorthingCoordinate": "59.3337783076748",
        "LocationEastingCoordinate": "18.0374650139904",
        "ZoneShortName": "A",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2020-09-28 00:00:00.000",
        "ExistsFromDate": "2020-09-28 00:00:00.000"
    },
    {
        "StopPointNumber": "10013",
        "StopPointName": "S:t Eriksgatan",
        "StopAreaNumber": "10008",
        "LocationNorthingCoordinate": "59.3347385640368",
        "LocationEastingCoordinate": "18.0305213894849",
        "ZoneShortName": "A",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2018-02-15 00:00:00.000",
        "ExistsFromDate": "2018-02-15 00:00:00.000"
    },
    {
        "StopPointNumber": "10014",
        "StopPointName": "Scheelegatan",
        "StopAreaNumber": "10014",
        "LocationNorthingCoordinate": "59.3329949649964",
        "LocationEastingCoordinate": "18.0442783066394",
        "ZoneShortName": "A",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2018-08-20 00:00:00.000",
        "ExistsFromDate": "2018-08-20 00:00:00.000"
    },
    {
        "StopPointNumber": "10015",
        "StopPointName": "Scheelegatan",
        "StopAreaNumber": "10014",
        "LocationNorthingCoordinate": "59.3326275058485",
        "LocationEastingCoordinate": "18.0462850270573",
        "ZoneShortName": "A",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2020-01-23 00:00:00.000",
        "ExistsFromDate": "2020-01-23 00:00:00.000"
    },
    {
        "StopPointNumber": "10016",
        "StopPointName": "Kungsbroplan",
        "StopAreaNumber": "10016",
        "LocationNorthingCoordinate": "59.3323982898700",
        "LocationEastingCoordinate": "18.0488034046866",
        "ZoneShortName": "A",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2014-06-03 00:00:00.000",
        "ExistsFromDate": "2014-06-03 00:00:00.000"
    },
    {
        "StopPointNumber": "10593",
        "StopPointName": "Danvikstull",
        "StopAreaNumber": "10593",
        "LocationNorthingCoordinate": "59.3139133167104",
        "LocationEastingCoordinate": "18.1007433223873",
        "ZoneShortName": "A",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2014-06-03 00:00:00.000",
        "ExistsFromDate": "2014-06-03 00:00:00.000"
    },
    {
        "StopPointNumber": "10594",
        "StopPointName": "Londonviadukten",
        "StopAreaNumber": "10591",
        "LocationNorthingCoordinate": "59.3146917025166",
        "LocationEastingCoordinate": "18.0977016085400",
        "ZoneShortName": "A",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2016-11-02 00:00:00.000",
        "ExistsFromDate": "2016-11-02 00:00:00.000"
    },
    {
        "StopPointNumber": "10595",
        "StopPointName": "Henriksdal",
        "StopAreaNumber": "10595",
        "LocationNorthingCoordinate": "59.3118649933198",
        "LocationEastingCoordinate": "18.1069299969984",
        "ZoneShortName": "A",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2017-06-15 00:00:00.000",
        "ExistsFromDate": "2017-06-15 00:00:00.000"
    },
    {
        "StopPointNumber": "10596",
        "StopPointName": "Danviksklippan",
        "StopAreaNumber": "10596",
        "LocationNorthingCoordinate": "59.3126349746497",
        "LocationEastingCoordinate": "18.1063833237294",
        "ZoneShortName": "A",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2014-06-03 00:00:00.000",
        "ExistsFromDate": "2014-06-03 00:00:00.000"
    },
    {
        "StopPointNumber": "10597",
        "StopPointName": "Slussen",
        "StopAreaNumber": "10149",
        "LocationNorthingCoordinate": "59.3195088675330",
        "LocationEastingCoordinate": "18.0722374400517",
        "ZoneShortName": "A",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2020-05-09 00:00:00.000",
        "ExistsFromDate": "2020-05-09 00:00:00.000"
    },
    {
        "StopPointNumber": "10598",
        "StopPointName": "Saltsjöqvarn",
        "StopAreaNumber": "10598",
        "LocationNorthingCoordinate": "59.3143200141482",
        "LocationEastingCoordinate": "18.1110400638340",
        "ZoneShortName": "A",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2014-06-03 00:00:00.000",
        "ExistsFromDate": "2014-06-03 00:00:00.000"
    },
    {
        "StopPointNumber": "10599",
        "StopPointName": "Henriksdalsviadukten",
        "StopAreaNumber": "10599",
        "LocationNorthingCoordinate": "59.3118642668592",
        "LocationEastingCoordinate": "18.1095863168913",
        "ZoneShortName": "A",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2016-11-02 00:00:00.000",
        "ExistsFromDate": "2016-11-02 00:00:00.000"
    },
    {
        "StopPointNumber": "10600",
        "StopPointName": "Henriksdal",
        "StopAreaNumber": "10595",
        "LocationNorthingCoordinate": "59.3122766926727",
        "LocationEastingCoordinate": "18.1078250130065",
        "ZoneShortName": "A",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2017-06-15 00:00:00.000",
        "ExistsFromDate": "2017-06-15 00:00:00.000"
    },
    {
        "StopPointNumber": "40493",
        "StopPointName": "Sarvträsk",
        "StopAreaNumber": "40493",
        "LocationNorthingCoordinate": "59.3281500089119",
        "LocationEastingCoordinate": "18.2622239998841",
        "ZoneShortName": "B",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2014-06-03 00:00:00.000",
        "ExistsFromDate": "2014-06-03 00:00:00.000"
    },
    {
        "StopPointNumber": "40494",
        "StopPointName": "Sarvträsk",
        "StopAreaNumber": "40493",
        "LocationNorthingCoordinate": "59.3290561720134",
        "LocationEastingCoordinate": "18.2627463501637",
        "ZoneShortName": "B",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2014-06-03 00:00:00.000",
        "ExistsFromDate": "2014-06-03 00:00:00.000"
    },
    {
        "StopPointNumber": "40496",
        "StopPointName": "Förmansvägen",
        "StopAreaNumber": "40491",
        "LocationNorthingCoordinate": "59.3298351875081",
        "LocationEastingCoordinate": "18.2794810652473",
        "ZoneShortName": "B",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2014-06-03 00:00:00.000",
        "ExistsFromDate": "2014-06-03 00:00:00.000"
    },
    {
        "StopPointNumber": "40508",
        "StopPointName": "Orminge centrum",
        "StopAreaNumber": "40273",
        "LocationNorthingCoordinate": "59.3256483636259",
        "LocationEastingCoordinate": "18.2579200805430",
        "ZoneShortName": "B",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2015-09-24 00:00:00.000",
        "ExistsFromDate": "2015-09-24 00:00:00.000"
    },
    {
        "StopPointNumber": "40510",
        "StopPointName": "Orminge centrum",
        "StopAreaNumber": "40273",
        "LocationNorthingCoordinate": "59.3265586424234",
        "LocationEastingCoordinate": "18.2585066574991",
        "ZoneShortName": "B",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2015-09-24 00:00:00.000",
        "ExistsFromDate": "2015-09-24 00:00:00.000"
    },
    {
        "StopPointNumber": "40420",
        "StopPointName": "Sandholmsvägen",
        "StopAreaNumber": "40419",
        "LocationNorthingCoordinate": "59.3294892932441",
        "LocationEastingCoordinate": "18.2730939020979",
        "ZoneShortName": "B",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2014-06-03 00:00:00.000",
        "ExistsFromDate": "2014-06-03 00:00:00.000"
    },
    {
        "StopPointNumber": "40422",
        "StopPointName": "Korset",
        "StopAreaNumber": "40421",
        "LocationNorthingCoordinate": "59.3327080365017",
        "LocationEastingCoordinate": "18.2648204275498",
        "ZoneShortName": "B",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2014-06-03 00:00:00.000",
        "ExistsFromDate": "2014-06-03 00:00:00.000"
    },
    {
        "StopPointNumber": "40431",
        "StopPointName": "Myrsjöskolan",
        "StopAreaNumber": "40431",
        "LocationNorthingCoordinate": "59.3316087890907",
        "LocationEastingCoordinate": "18.2716657930054",
        "ZoneShortName": "B",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2014-06-03 00:00:00.000",
        "ExistsFromDate": "2014-06-03 00:00:00.000"
    },
    {
        "StopPointNumber": "40432",
        "StopPointName": "Myrsjöskolan",
        "StopAreaNumber": "40431",
        "LocationNorthingCoordinate": "59.3320206537679",
        "LocationEastingCoordinate": "18.2703071480519",
        "ZoneShortName": "B",
        "StopAreaTypeCode": "BUSTERM",
        "LastModifiedUtcDateTime": "2014-06-03 00:00:00.000",
        "ExistsFromDate": "2014-06-03 00:00:00.000"
    }
]

export const listOfStops = [
    {
        "LineNumber": "1",
        "DirectionCode": "1",
        "JourneyPatternPointNumber": "10008",
        "LastModifiedUtcDateTime": "2018-02-16 00:00:00.000",
        "ExistsFromDate": "2018-02-16 00:00:00.000"
    },
    {
        "LineNumber": "1",
        "DirectionCode": "1",
        "JourneyPatternPointNumber": "10012",
        "LastModifiedUtcDateTime": "2020-09-28 00:00:00.000",
        "ExistsFromDate": "2020-09-28 00:00:00.000"
    },
    {
        "LineNumber": "1",
        "DirectionCode": "1",
        "JourneyPatternPointNumber": "10014",
        "LastModifiedUtcDateTime": "2018-08-20 00:00:00.000",
        "ExistsFromDate": "2018-08-20 00:00:00.000"
    },
    {
        "LineNumber": "446",
        "DirectionCode": "2",
        "JourneyPatternPointNumber": "40422",
        "LastModifiedUtcDateTime": "2012-06-23 00:00:00.000",
        "ExistsFromDate": "2012-06-23 00:00:00.000"
    },
    {
        "LineNumber": "446",
        "DirectionCode": "2",
        "JourneyPatternPointNumber": "40494",
        "LastModifiedUtcDateTime": "2012-06-23 00:00:00.000",
        "ExistsFromDate": "2012-06-23 00:00:00.000"
    },
    {
        "LineNumber": "446",
        "DirectionCode": "2",
        "JourneyPatternPointNumber": "40508",
        "LastModifiedUtcDateTime": "2014-06-03 00:00:00.000",
        "ExistsFromDate": "2014-06-03 00:00:00.000"
    },
    {
        "LineNumber": "447",
        "DirectionCode": "2",
        "JourneyPatternPointNumber": "10594",
        "LastModifiedUtcDateTime": "2016-11-02 00:00:00.000",
        "ExistsFromDate": "2016-11-02 00:00:00.000"
    },
    {
        "LineNumber": "447",
        "DirectionCode": "2",
        "JourneyPatternPointNumber": "10600",
        "LastModifiedUtcDateTime": "2017-03-10 00:00:00.000",
        "ExistsFromDate": "2017-03-10 00:00:00.000"
    }
]