import reducer from '../reducer';
import {
    GET_STOPS_FAIL, 
    GET_STOPS_SUCCESS,
    GET_STOPS_INFO_FAIL, 
    GET_STOPS_INFO_SUCCESS 
} from "../actions";
import { listOfStops, stopsData } from "./testData/homeTestData";

describe('Home reducer', () => {
    let initialState = {};

    beforeEach(() => {
        initialState = {
            loading: false,
            listOfStops: {},
            listOfStopInfo: {},
            error: ''
        }
    });

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState)
    });

    it('should handle GET_STOPS_SUCCESS', () => {
        expect(
            reducer([], {
                type: GET_STOPS_SUCCESS,
                payload: {
                    data: {
                        ResponseData: {
                            Result: listOfStops
                        }
                    }
                }
            })
        ).toEqual(
            {
                listOfStops: listOfStops,
                "loading": false,
            }
        );
    });

    it('should handle GET_STOPS_FAIL', () => {
        expect(
            reducer([], {
                type: GET_STOPS_FAIL,
                payload: {}
            })
        ).toEqual(
            {
                loading: false,
                error: 'Unable to Fetch Data'
            }
        );
    });

    it('should handle GET_STOPS_INFO_SUCCESS', () => {
        expect(
            reducer([], {
                type: GET_STOPS_INFO_SUCCESS,
                payload: {
                    data: {
                        ResponseData: {
                            Result: stopsData
                        }
                    }
                }
            })
        ).toEqual(
            {
                listOfStopInfo: stopsData,
                "loading": false,
            }
        );
    });

    it('should handle GET_STOPS_INFO_FAIL', () => {
        expect(
            reducer([], {
                type: GET_STOPS_INFO_FAIL,
                payload: {}
            })
        ).toEqual(
            {
                loading: false,
                error: 'Unable to Fetch Data'
            }
        );
    });
})