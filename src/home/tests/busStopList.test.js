import React from 'react';
import { shallow } from 'enzyme';
import BusStopList from '../busStopList';
import { busStopList } from "./testData/busStopListTestData";

describe('Home', () => {
    it('should render correctly without the data', () => {
        const component = shallow(<BusStopList busStopList={busStopList}/>);

        expect(component).toMatchSnapshot();
    });
});