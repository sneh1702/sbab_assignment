export const getformattedBusStopData = (listOfStops, stopsData) => {
    let listOfStopsByRoute = {};

    listOfStopsByRoute = listOfStops.reduce(function (res, stopObject) {
        res[stopObject.LineNumber] = res[stopObject.LineNumber] || [];
        res[stopObject.LineNumber].push(stopObject.JourneyPatternPointNumber);
        return res;
    }, Object.create(null));

    const sortedStopList = sortBusRoutesByStops(listOfStopsByRoute);

    const top10Routes = getTopTenRoutesByMaxStops(sortedStopList);

    const requiredStopsNumbers = getRequiredStopsList(top10Routes);

    const requiredStopsNames =  getRequiredStopsNamesList(requiredStopsNumbers, stopsData);
    
    const top10RoutesWithNames = getTopTenRoutesWithStopNames(top10Routes, requiredStopsNames)

    return top10RoutesWithNames;
}

const sortBusRoutesByStops = (busRoutes) => {
    const result = Object.keys(busRoutes)
        .map(function (route) {
            return {
                key: route,
                value: Array.from(new Set(busRoutes[route]))
            }
        })
        .sort(function (route1, route2) {
            return route2.value.length - route1.value.length
        });
    
    return result;
}

const getTopTenRoutesByMaxStops = (sortedRoutesArray) => {
    const result = sortedRoutesArray.slice(0,10);

    return result;
}

const getRequiredStopsList = (sortedStopList) => {
    let result = [];

    sortedStopList.map(stop => (
        result = result.concat(stop.value)
    ));

    result = Array.from(new Set(result));

    return result;
}

const getRequiredStopsNamesList = (requiredStopsNumbers, stopsData) => {
    let result = {};

    stopsData.map(stop => {
        if(requiredStopsNumbers.includes(stop.StopPointNumber)){
            result[stop.StopPointNumber] = stop.StopPointName
        }

        return {}
    });

    return result;
}

const getTopTenRoutesWithStopNames = (top10Routes, requiredStopsNames) => {
    let result = [];
    debugger;

    result = top10Routes.map(route => {
        let routeStops = [];
        routeStops = Array.from(route.value).map(stop => {
            return requiredStopsNames[stop]
        })

        return{
            [route.key]: Array.from(new Set(routeStops)),
        }
    });

    return result;
}