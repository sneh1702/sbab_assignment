import { GET_STOPS, GET_STOPS_FAIL, GET_STOPS_SUCCESS, GET_STOPS_INFO, GET_STOPS_INFO_FAIL, GET_STOPS_INFO_SUCCESS } from "../actions";

const initialState = {
    loading: false,
    listOfStops: {},
    listOfStopInfo: {},
    error: ''
}

export default function HomeReducer(state=initialState, action){
    switch(action.type){
        case GET_STOPS: {
            return({
                ...state,
                loading: true
            });
        }
        case GET_STOPS_SUCCESS: {
            // alert('yippey');
            return({
                ...state,
                loading: false,
                listOfStops: action.payload.data.ResponseData.Result
            });
        }
        case GET_STOPS_FAIL: {
            return({
                ...state,
                loading: false,
                error: 'Unable to Fetch Data'
            });
        }
        case GET_STOPS_INFO: {
            return({
                ...state,
                loading: true
            });
        }
        case GET_STOPS_INFO_SUCCESS: {
            // alert('yippey2');
            return({
                ...state,
                loading: false,
                listOfStopInfo: action.payload.data.ResponseData.Result
            });
        }
        case GET_STOPS_INFO_FAIL: {
            return({
                ...state,
                loading: false,
                error: 'Unable to Fetch Data'
            });
        }
        default: 
            return(initialState);
    }
}