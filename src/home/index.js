import React, {Component} from 'react';
import { connect } from "react-redux";

import { getListOfStops, getStopsInfo } from "./actions";
import { getformattedBusStopData } from "./homeContainer";
import BusStopList from './busStopList';
import './styles/homeStyles.scss';

class Home extends Component{
    state={}

    componentDidMount() {
        const {
            getListOfStops,
            getStopsInfo
        } = this.props;

        getListOfStops();
        getStopsInfo();
    }

    static getDerivedStateFromProps(props, state) {
        if(props.listOfStops.length > 0 && props.stopsData.length > 0){
            const busStopList = getformattedBusStopData(props.listOfStops, props.stopsData);
            return {
                busStopList: busStopList
            }
        }
        else{
            return {}
        }
    }

    render(){
        const {error} = this.props;

        if(error){
            return (<div>{error}</div>)
        }
        else{
            return(
                <div className='home'>
                    {this.state.busStopList ? <BusStopList busStopList = {this.state.busStopList}/> : <div>Loading......</div>}
                </div>
            );
        }        
    }
}

const mapStateToProps = (state) => {
	return ({
		loading: state.homeReducer.loading,
		listOfStops: state.homeReducer.listOfStops,
        stopsData: state.homeReducer.listOfStopInfo,
        error: state.homeReducer.error
	});
}

const mapDispatchToProps = (dispatch) => {
	return({
		getListOfStops: () => dispatch(getListOfStops()),
		getStopsInfo: () => dispatch(getStopsInfo())
	});
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);