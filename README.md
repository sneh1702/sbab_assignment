This is an assignment given by SBAB for the recruitment process for the role for a Frontend Devleoper. The task was to access the Publically available APIs of Trafiklab and display the 10 bus lines with most number of stops, along with the stop names.

## Application Features

As mentioned above, the api given in the assignment https://www.trafiklab.se/ is hit and the resultant data is processed to calculate the 10 bus lines with the maximum stops.

There are 2 APIs to be hit, first to fetch all the bus lines , and the second to fetch all the data related to the stops.

The resultant data from the first api is then manipulated to get the top 10 bus lines with all the stop IDs for each line, and the 2nd API data is then consulted to get the names of all the bus stops present in the processed data.


## Running the Application on local machine

In order to run the application locally, the following steps have to be performed:

- Clone this repository.
- Open a command prompt/cmd window pointing to the root folder and run ```npm install``` (details explained below)
- Once the above step is completed successfully, run ```npm start```. This will start the application on your default browser.
- In order to run the test cases, ```npm run test``` command has to be executed on the cmd.

### `npm install`

Installs all the dependencies required by the application to work. Running this command fetches all the dependencies from NPM's registry and saves it in the node_modules folder so that those are available for the application to use.

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run test`

Launches the test runner in the interactive watch mode by default, however I have stopped the watcher for this application.<br />

I have achieved a 100% coverage for this application, the screenshot for which can be found below in the screenshots section.


## Screenshot

![Application Screenshot](readme_screenshots/Home.png)

![Test Coverage Screenshot](readme_screenshots/Test_Coverage.png)